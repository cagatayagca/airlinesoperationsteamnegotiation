package edu.ozu.aocteamnegotiation;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import edu.ozu.aocteamnegotiation.model.base.CrewMember;
import edu.ozu.aocteamnegotiation.model.base.Disruption;
import edu.ozu.aocteamnegotiation.model.base.Flight;
import edu.ozu.aocteamnegotiation.model.base.FlightCost;
import edu.ozu.aocteamnegotiation.util.CrewScheduleJsonParser;
import edu.ozu.aocteamnegotiation.util.DisruptionJsonParser;
import edu.ozu.aocteamnegotiation.util.FlightCostsJsonParser;
import edu.ozu.aocteamnegotiation.util.FlightScheduleJsonParser;

@SpringBootApplication
public class AocTeamNegotiationApplication {

	private static final Logger LOGGER = LoggerFactory.getLogger(AocTeamNegotiationApplication.class);

	@PostConstruct
	void init() {
		System.out.println("post construct ran.");
		LOGGER.info("test log");
		FlightScheduleJsonParser.parseFlightScheduleJson("src/main/resources/FlightSchedule.json");
		FlightCostsJsonParser.parseFlightCostJson("src/main/resources/FlightCosts.json");
		CrewScheduleJsonParser.parseCrewScheduleJson("src/main/resources/CrewSchedule.json");
	}

	public static void main(String[] args) {
		SpringApplication.run(AocTeamNegotiationApplication.class, args);
		for (Flight flight2 : FlightScheduleJsonParser.SCHEDULED_FLIGHT_LIST) {
			System.out.println(flight2);
		}
		for (FlightCost cost : FlightCostsJsonParser.FLIGHT_COSTS_MAP.values()) {
			System.out.println(cost);
		}
		if (DisruptionJsonParser.parseDisruptionJson("src/main/resources/Disruption.json").isPresent()) {
			System.out.println("ispresent");
			Disruption d = DisruptionJsonParser.parseDisruptionJson("src/main/resources/Disruption.json").get();
			System.out.println(d);
		}
		for (CrewMember member : CrewScheduleJsonParser.SCHEDULED_CREW_LIST) {
			System.out.println(member);
		}
		// if
		// (PaxJsonParser.parsePaxJson("src/main/resources/DisruptedPax.json").isPresent())
		// {
		// System.out.println(PaxJsonParser.parsePaxJson("src/main/resources/DisruptedPax.json").get());
		// }

	}

}
