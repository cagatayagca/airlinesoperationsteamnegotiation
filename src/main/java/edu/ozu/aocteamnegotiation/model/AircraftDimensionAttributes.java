package edu.ozu.aocteamnegotiation.model;

import java.util.ArrayList;

import edu.ozu.aocteamnegotiation.model.base.DimensionAttribute;
import edu.ozu.aocteamnegotiation.model.base.Flight;
import edu.ozu.aocteamnegotiation.model.constants.Constants;

public class AircraftDimensionAttributes extends DimensionAttribute {

	private ArrayList<Flight> aircraftPartialSolution;

	public ArrayList<Flight> getAircraftPartialSolution() {
		return aircraftPartialSolution;
	}

	public void setAircraftPartialSolution(ArrayList<Flight> aircraftPartialSolution) {
		this.aircraftPartialSolution = aircraftPartialSolution;
	}

	@Override
	public String toString() {
		return Constants.AIRCRAFT_DIM_ATTR_PREFIX + super.toString();
	}
}
