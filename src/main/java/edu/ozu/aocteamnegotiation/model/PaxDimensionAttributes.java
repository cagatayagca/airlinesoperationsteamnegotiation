package edu.ozu.aocteamnegotiation.model;

import edu.ozu.aocteamnegotiation.model.base.DimensionAttribute;
import edu.ozu.aocteamnegotiation.model.constants.Constants;

public class PaxDimensionAttributes extends DimensionAttribute {
	// TODO

	@Override
	public String toString() {
		return Constants.PAX_DIM_ATTR_PREFIX + super.toString();
	}
}
