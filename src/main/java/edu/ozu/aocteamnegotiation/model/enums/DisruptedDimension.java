package edu.ozu.aocteamnegotiation.model.enums;

public enum DisruptedDimension {

	AIRCRAFT(1), CREW(2), PAX(3);

	private int id;

	DisruptedDimension(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

}
