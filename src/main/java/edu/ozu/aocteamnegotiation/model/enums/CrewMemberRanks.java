package edu.ozu.aocteamnegotiation.model.enums;

public enum CrewMemberRanks {
	CAPTAIN(12, 70, 3), OFFICER_2ND(12, 60, 2), PURSER(6, 40, 1), FLIGHT_ATTENDANT(6, 30, 1);

	private int dailyMaxHours;
	private int hourlyRate;
	private int extraHourRate;

	private CrewMemberRanks(int maxHours, int rate, int extraHourRate) {
		this.dailyMaxHours = maxHours;
		this.hourlyRate = rate;
		this.extraHourRate = extraHourRate;
	}

	public int getDailyMaxHours() {
		return dailyMaxHours;
	}

	public int getHourlyRate() {
		return hourlyRate;
	}

	public int getExtraHourRate() {
		return extraHourRate;
	}

}
