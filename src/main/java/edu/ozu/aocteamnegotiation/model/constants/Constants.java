package edu.ozu.aocteamnegotiation.model.constants;

import java.time.format.DateTimeFormatter;

import com.google.gson.Gson;

public final class Constants {
	private Constants() {
	}

	public static final String AIRCRAFT_DIM_ATTR_PREFIX = "AircraftDimensionAttr";
	public static final String CREW_DIM_ATTR_PREFIX = "CrewDimensionAttr";
	public static final String PAX_DIM_ATTR_PREFIX = "PaxDimensionAttr";
	public static final DateTimeFormatter DATETIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
	public static final Gson GSON = new Gson();
	public static final String FLIGHT_NUMBER = "flightNumber";
	public static final String DEPARTURE_AIRPORT = "depAirport";
	public static final String ARRIVAL_AIRPORT = "arrAirport";
	public static final String SCHEDULED_TIME_OF_DEP = "scheduledTimeOfDep";
	public static final String SCHEDULED_TIME_OF_ARR = "scheduledTimeOfArr";
	public static final String AIRCRAFT = "aircraft";
	public static final String ESTIMATED_TIME_OF_DEP = "estimatedTimeOfDep";
	public static final String ESTIMATED_TIME_OF_ARR = "estimatedTimeOfArr";
	public static final String FLEET_NUM = "fleetNumber";
	public static final String TAIL_NUMBER = "tailNumber";
	public static final String TOTAL_AVAIL_SEATS = "totalAvailSeats";
	public static final String EMPTY_STRING = "";
	public static final String DISRUPTED_FLIGHT = "disruptedFlight";
	public static final String AIRPORT = "airport";
	public static final String TAKEOFF = "takeOff";
	public static final String LANDING = "landing";
	public static final String PARKING = "parking";
	public static final String HANDLING = "handling";
	public static final String MAINT = "maintenance";
	public static final String ATCCHARGES = "airTrafficControlCharges";
	public static final String FUEL = "fuel";
	public static final String SOLD_SEATS = "soldSeats";
	public static final String PAX_ID = "paxId";
	public static final String DEST_AIRPORT = "destAirport";
	public static final String SCHEDULED_TRIP_HOURS = "scheduledTripHours";
	public static final String TIME_OF_ARR_OF_LAST_FLIGHT = "timeOfArrOfLastFlight";
	public static final String TIME_OF_DEP_OF_FIRST_FLIGHT = "timeOfDepOfFirstFlight";
	public static final String DISRUPTED_PAX_LIST = "disruptedPaxList";
	public static final String MEMBER_ID = "memberId";
	public static final String HOMEBASE = "homeBase";
	public static final String READINESS_TIME = "readinessTime";
	public static final String CAPTAIN = "CAPTAIN";
	public static final String OFFICER_1ST = "OFFICER_1ST";
	public static final String PURSER = "PURSER";
	public static final String FLIGHT_ATTENDANT = "FLIGHT_ATTENDANT";
	public static final String RANK = "rank";
	public static final String DAILY_HOURS = "dailyHours";
	public static final String ACTIVITY = "activity";
	public static final String STARTING_LOC = "startingLoc";
	public static final String ESTIMATED_START_TIME = "estStartTime";
	public static final String EST_END_TIME = "estEndTime";
	public static final String SCHEDULED_START_TIME = "scheduledStartTime";
	public static final String SCHEDULED_END_TIME = "scheduledEndTime";
	public static final String DISRUPTED_CREW = "disruptedCrew";
	public static final int BIGGER_THAN = 1;

}
