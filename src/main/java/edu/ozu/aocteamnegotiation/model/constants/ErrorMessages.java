package edu.ozu.aocteamnegotiation.model.constants;

public final class ErrorMessages {
	private ErrorMessages() {

	}

	public static final String AIRCRAFT_MODEL_NULL_MSG = "Aircraft Model cannot be null!";
	public static final String FLIGHT_SCHEDULE_JSON_OPENING_ERR_MSG = "Error while opening the flight schedule json file. {}";
	public static final String FLIGHT_SCHEDULE_JSON_READING_ERR_MSG = "Error while reading flight schedule json file. {}";
	public static final String FLIGHT_SCHEDULE_JSON_UNKNOWN_ERR_MSG = "Uknown error occured while parsing FlightSchedule.json {}";
	public static final String DISRUPTION_JSON_OPENING_ERR_MSG = "Error while opening the disruption json file. {}";
	public static final String DISRUPTION_JSON_READING_ERR_MSG = "Error while reading disruption json file. {}";
	public static final String DISRUPTION_JSON_UNKNOWN_ERR_MSG = "Uknown error occured while parsing Disruption.json {}";
	public static final String FLIGHT_JSON_NULL_RUNTIME_ERR_MSG = "Flight json element is null!";
	public static final String FLIGHT_SCHEDULE_JSON_SYNTAX_ERR_MSG = "Error while parsing the flight schedule json file. {}";
	public static final String DISRUPTION_JSON_SYNTAX_ERR_MSG = "Error while parsing the disruption json file. {}";
	public static final String NULL_FLIGHT_RUNTIME_ERR_MSG = "Flight parser returned null!";
	public static final String NULL_DISRUPTION_CREATION_ERR_MSG = "Disruption fields cannot be null. Only may be empty!";
	public static final String FLIGHT_COST_JSON_OPENING_ERR_MSG = "Error while opening the flight cost json file. {}";
	public static final String FLIGHT_COST_JSON_READING_ERR_MSG = "Error while reading flight cost json file. {}";
	public static final String FLIGHT_COST_JSON_UNKNOWN_ERR_MSG = "Uknown error occured while parsing FlightCost.json {}";
	public static final String PAX_JSON_EMPTY_ERR_MSG = "Pax JSON array is null!";
	public static final String CREW_JSON_PARSE_ERROR = "Error while parsing the crew schedule! {}";
	public static final String CREW_JSON_EMPTY_ERR_MSG = "Crew JSON array is null!";
	public static final String CREW_SCHEDULE_JSON_OPENING_ERR_MSG = "Error while opening the crew schedule json file. {}";
	public static final String CREW_SCHEDULE_JSON_READING_ERR_MSG = "Error while reading crew schedule json file. {}";
	public static final String CREW_SCHEDULE_JSON_UNKNOWN_ERR_MSG = "Uknown error occured while parsing CrewSchedule.json {}";
	public static final String CREW_SCHEDULE_JSON_SYNTAX_ERR_MSG = "Error while parsing the crew schedule json file. {}";
	public static final String NULL_CREW_RUNTIME_ERR_MSG = "Crew parser returned null!";
}
