package edu.ozu.aocteamnegotiation.model.base;

import java.util.ArrayList;
import java.util.List;

import edu.ozu.aocteamnegotiation.model.constants.ErrorMessages;

public class Disruption {

	private Flight disruptedFlight;
	private ArrayList<CrewMember> disruptedCrew;
	private ArrayList<Passenger> disruptedPaxList;

	public Disruption(Flight dFlight, List<CrewMember> dCrew, List<Passenger> dPaxList) {
		if (dFlight == null || dCrew == null || dPaxList == null) {
			throw new RuntimeException(ErrorMessages.NULL_DISRUPTION_CREATION_ERR_MSG);
		}
		this.disruptedFlight = dFlight;
		this.disruptedCrew = new ArrayList<>(dCrew);
		this.disruptedPaxList = new ArrayList<>(dPaxList);
	}

	@SuppressWarnings("unused")
	private Disruption() {
	}

	public Flight getDisruptedFlight() {
		return disruptedFlight;
	}

	public void setDisruptedFlight(Flight disruptedFlight) {
		this.disruptedFlight = disruptedFlight;
	}

	public ArrayList<CrewMember> getDisruptedCrew() {
		return disruptedCrew;
	}

	public void setDisruptedCrew(ArrayList<CrewMember> disruptedCrew) {
		this.disruptedCrew = disruptedCrew;
	}

	public ArrayList<Passenger> getDisruptedPaxList() {
		return disruptedPaxList;
	}

	public void setDisruptedPaxList(ArrayList<Passenger> disruptedPaxList) {
		this.disruptedPaxList = disruptedPaxList;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		if (this.disruptedFlight != null) {
			builder.append("[disruptedFlight:").append(this.disruptedFlight.toString())
					.append("|#OfDisruptedCrewMember:").append(this.disruptedCrew.size()).append("|#OfDisruptedPax:")
					.append(this.disruptedPaxList.size()).append("]");
		}

		return builder.toString();
	}

}
