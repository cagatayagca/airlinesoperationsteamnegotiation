package edu.ozu.aocteamnegotiation.model.base;

import java.math.BigDecimal;

public class FlightCost {
	private String airport;
	private BigDecimal takeOff;
	private BigDecimal landing;
	private BigDecimal parking;
	private BigDecimal handling;
	private BigDecimal maintenance;
	private BigDecimal airTrafficControlCharges;
	private BigDecimal fuel;

	private FlightCost() {
		this.airTrafficControlCharges = BigDecimal.ZERO;
		this.fuel = BigDecimal.ZERO;
		this.handling = BigDecimal.ZERO;
		this.landing = BigDecimal.ZERO;
		this.maintenance = BigDecimal.ZERO;
		this.parking = BigDecimal.ZERO;
		this.takeOff = BigDecimal.ZERO;
	}

	public static class FlightCostBuilder {
		private FlightCost cost;

		public FlightCostBuilder(final String airport) {
			this.cost = new FlightCost();
			this.cost.airport = airport;
		}

		public FlightCostBuilder takeOff(BigDecimal takeOffCharges) {
			this.cost.takeOff = takeOffCharges;
			return this;
		}

		public FlightCostBuilder landing(BigDecimal langindCharges) {
			this.cost.landing = langindCharges;
			return this;
		}

		public FlightCostBuilder parking(BigDecimal parkingCharges) {
			this.cost.parking = parkingCharges;
			return this;
		}

		public FlightCostBuilder handling(BigDecimal handlingCharges) {
			this.cost.handling = handlingCharges;
			return this;
		}

		public FlightCostBuilder maintenance(BigDecimal maintenanceCharges) {
			this.cost.maintenance = maintenanceCharges;
			return this;
		}

		public FlightCostBuilder airTrafficContCharges(BigDecimal atcCharges) {
			this.cost.airTrafficControlCharges = atcCharges;
			return this;
		}

		public FlightCostBuilder fuel(BigDecimal fuelCharges) {
			this.cost.fuel = fuelCharges;
			return this;
		}

		public FlightCost build() {
			return this.cost;
		}

	}

	public String getAirport() {
		return airport;
	}

	public BigDecimal getTakeOff() {
		return takeOff;
	}

	public BigDecimal getLanding() {
		return landing;
	}

	public BigDecimal getParking() {
		return parking;
	}

	public BigDecimal getHandling() {
		return handling;
	}

	public BigDecimal getMaintenance() {
		return maintenance;
	}

	public BigDecimal getAirTrafficControlCharges() {
		return airTrafficControlCharges;
	}

	public BigDecimal getFuel() {
		return fuel;
	}

	public BigDecimal totalFlightCost() {
		return this.airTrafficControlCharges.add(this.fuel).add(this.handling).add(this.landing).add(this.maintenance)
				.add(this.parking).add(this.takeOff);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("[airport:").append(this.airport).append("|landing:").append(this.landing.toString())
				.append("|parking:").append(this.parking.toString()).append("|fuel:").append(this.fuel.toString())
				.append("|handling:").append(this.handling.toString()).append("|maintenance:")
				.append(this.maintenance.toString()).append("|ATC:").append(this.airTrafficControlCharges.toString())
				.append("|takeOff:").append(this.takeOff.toString()).append("]");
		return builder.toString();
	}

}
