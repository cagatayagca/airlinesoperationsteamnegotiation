package edu.ozu.aocteamnegotiation.model.base;

import java.math.BigDecimal;

public abstract class DimensionAttribute {

	private long delay;
	private BigDecimal cost;
	private int penalty;

	public long getDelay() {
		return delay;
	}

	public void setDelay(long delay) {
		this.delay = delay;
	}

	public BigDecimal getCost() {
		return cost;
	}

	public void setCost(BigDecimal cost) {
		this.cost = cost;
	}

	public int getPenalty() {
		return penalty;
	}

	public void setPenalty(int penalty) {
		this.penalty = penalty;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("|delay:").append(String.valueOf(delay)).append("|cost:").append(cost.toString())
				.append("|penalty:").append(this.penalty);
		return builder.toString();
	}

	@Override
	public boolean equals(Object object) {
		if (object instanceof DimensionAttribute) {
			DimensionAttribute dimensionAttr = (DimensionAttribute) object;
			if (new BigDecimal(this.delay).compareTo(new BigDecimal(dimensionAttr.getDelay())) == 0
					&& this.cost.compareTo(dimensionAttr.getCost()) == 0 && this.penalty == dimensionAttr.penalty) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

}
