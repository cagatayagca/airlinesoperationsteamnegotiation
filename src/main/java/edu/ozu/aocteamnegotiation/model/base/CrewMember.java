package edu.ozu.aocteamnegotiation.model.base;

import java.time.LocalDateTime;

import edu.ozu.aocteamnegotiation.model.constants.Constants;
import edu.ozu.aocteamnegotiation.model.enums.CrewMemberRanks;

public class CrewMember {
	private int memberId;
	private CrewMemberRanks rank;
	private int fleetNumber;
	private String homeBase;
	private LocalDateTime readinessTime;
	private CrewActivity activity;
	private int dailyHours;

	private CrewMember() {
	}

	public static class CrewMemberBuilder {
		CrewMember member;

		public CrewMemberBuilder() {
			this.member = new CrewMember();
		}

		public CrewMemberBuilder memberId(int id) {
			this.member.memberId = id;
			return this;
		}

		public CrewMemberBuilder rank(CrewMemberRanks rank) {
			this.member.rank = rank;
			return this;
		}

		public CrewMemberBuilder fleetNo(int fleetNumber) {
			this.member.fleetNumber = fleetNumber;
			return this;
		}

		public CrewMemberBuilder homeBase(String home) {
			this.member.homeBase = home;
			return this;
		}

		public CrewMemberBuilder readinessTime(LocalDateTime time) {
			this.member.readinessTime = time;
			return this;
		}

		public CrewMemberBuilder activity(CrewActivity activity) {
			this.member.activity = activity;
			return this;
		}

		public CrewMemberBuilder dailyHours(int hours) {
			this.member.dailyHours = hours;
			return this;
		}

		public CrewMember build() {
			if (this.member.homeBase == null) {
				this.member.homeBase = Constants.EMPTY_STRING;
			}
			return this.member;
		}
	}

	public int getMemberId() {
		return memberId;
	}

	public CrewMemberRanks getRank() {
		return rank;
	}

	public int getFleetNumber() {
		return fleetNumber;
	}

	public String getHomeBase() {
		return homeBase;
	}

	public LocalDateTime getReadinessTime() {
		return readinessTime;
	}

	public CrewActivity getActivity() {
		return activity;
	}

	public void setActivity(CrewActivity activity) {
		this.activity = activity;
	}

	public int getDailyHours() {
		return dailyHours;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof CrewMember) {
			CrewMember member = (CrewMember) obj;
			if (this.fleetNumber == member.fleetNumber && this.memberId == member.memberId && this.rank == member.rank
					&& this.homeBase.toLowerCase().trim().equals(member.homeBase.toLowerCase().trim())) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("[CrewMemberID:").append(this.memberId).append("|fleetNum:").append(this.fleetNumber)
				.append("|rank:").append(this.rank).append("|homeBase:").append(this.homeBase).append("|dailyHours:")
				.append(this.dailyHours).append("|activity:")
				.append(this.activity == null ? Constants.EMPTY_STRING : this.activity.toString()).append("]");
		return builder.toString();
	}

}
