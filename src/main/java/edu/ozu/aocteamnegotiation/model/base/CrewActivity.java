package edu.ozu.aocteamnegotiation.model.base;

import java.time.LocalDateTime;

public class CrewActivity {
	private String startingLoc;
	private LocalDateTime scheduledStartTime;
	private LocalDateTime scheduledEndTime;
	private LocalDateTime estStartTime;
	private LocalDateTime estEndTime;

	private CrewActivity() {
	}

	public static class CrewActivityBuilder {
		private CrewActivity crewActivity;

		public CrewActivityBuilder() {
			this.crewActivity = new CrewActivity();
		}

		public CrewActivityBuilder startingLocation(String location) {
			this.crewActivity.startingLoc = location;
			return this;
		}

		public CrewActivityBuilder scheduledStartTime(LocalDateTime startTime) {
			this.crewActivity.scheduledStartTime = startTime;
			return this;
		}

		public CrewActivityBuilder scheduledEndTime(LocalDateTime endTime) {
			this.crewActivity.scheduledEndTime = endTime;
			return this;
		}

		public CrewActivityBuilder estStartTime(LocalDateTime estStartTime) {
			this.crewActivity.estStartTime = estStartTime;
			return this;
		}

		public CrewActivityBuilder estEndTime(LocalDateTime estEndTime) {
			this.crewActivity.estEndTime = estEndTime;
			return this;
		}

		public CrewActivity build() {
			return this.crewActivity;
		}

	}

	public String getStartingLoc() {
		return startingLoc;
	}

	public LocalDateTime getScheduledStartTime() {
		return scheduledStartTime;
	}

	public LocalDateTime getScheduledEndTime() {
		return scheduledEndTime;
	}

	public LocalDateTime getEstStartTime() {
		return estStartTime;
	}

	public LocalDateTime getEstEndTime() {
		return estEndTime;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof CrewActivity) {
			CrewActivity activity = (CrewActivity) obj;
			if ((activity.startingLoc != null && this.startingLoc != null)
					&& activity.startingLoc.equals(this.startingLoc)
					&& activity.scheduledStartTime.isEqual(this.scheduledStartTime)
					&& activity.scheduledEndTime.isEqual(this.scheduledEndTime)) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("[startingLocation:").append(this.startingLoc).append("|scheduledStartTime:")
				.append(this.scheduledStartTime.toString()).append("|scheduledEndTime:")
				.append(this.scheduledEndTime.toString()).append("|estStartTime:").append(this.estStartTime.toString())
				.append("|estEndTime:").append(this.estEndTime.toString()).append("]");
		return builder.toString();
	}

}
