package edu.ozu.aocteamnegotiation.model.base;

import edu.ozu.aocteamnegotiation.model.constants.Constants;

public class Aircraft {

	private int fleetNumber;
	private String tailNumber;

	@SuppressWarnings("unused")
	private Aircraft() {
	}

	public Aircraft(int fleetNum, String tailNum) {
		this.fleetNumber = fleetNum;
		if (tailNum == null) {
			this.tailNumber = Constants.EMPTY_STRING;
		}
		this.tailNumber = tailNum;
	}

	public int getFleetNumber() {
		return fleetNumber;
	}

	public void setFleetNumber(int fleetNumber) {
		this.fleetNumber = fleetNumber;
	}

	public String getTailNumber() {
		return tailNumber;
	}

	public void setTailNumber(String tailNumber) {
		this.tailNumber = tailNumber;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("[fleetNumber:" + this.fleetNumber).append("|tailNumber:" + this.tailNumber).append("]");
		return builder.toString();
	}

	@Override
	public boolean equals(Object object) {
		if (object instanceof Aircraft) {
			Aircraft aircraft = (Aircraft) object;
			if (this.tailNumber.equals(aircraft.getTailNumber()) && this.fleetNumber == aircraft.getFleetNumber()) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}

	}

}