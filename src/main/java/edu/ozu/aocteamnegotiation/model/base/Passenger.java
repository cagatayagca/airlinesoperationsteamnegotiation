package edu.ozu.aocteamnegotiation.model.base;

import java.math.BigInteger;
import java.time.LocalDateTime;

import edu.ozu.aocteamnegotiation.model.constants.Constants;

public class Passenger {
	private int paxId;
	private String destAirport;
	private BigInteger scheduledTripHours;
	private LocalDateTime timeOfArrOfLastFlight;
	private LocalDateTime timeOfDepOfFirstFlight;

	private Passenger() {
	}

	public static class PaxBuilder {
		private Passenger pax;

		public PaxBuilder(final int id) {
			this.pax = new Passenger();
			this.pax.paxId = id;
		}

		public PaxBuilder destinationAirp(String airport) {
			this.pax.destAirport = airport;
			return this;
		}

		public PaxBuilder scheduledTripHours(BigInteger hours) {
			this.pax.scheduledTripHours = hours;
			return this;
		}

		public PaxBuilder timeOfArrival(LocalDateTime timeOfArrival) {
			this.pax.timeOfArrOfLastFlight = timeOfArrival;
			return this;
		}

		public PaxBuilder timeOfDepature(LocalDateTime timeOfDeparture) {
			this.pax.timeOfDepOfFirstFlight = timeOfDeparture;
			return this;
		}

		public Passenger build() {
			if (this.pax.destAirport == null) {
				this.pax.destAirport = Constants.EMPTY_STRING;
			}
			return this.pax;
		}
	}

	public int getPaxId() {
		return paxId;
	}

	public String getDestAirport() {
		return destAirport;
	}

	public BigInteger getScheduledTripHours() {
		return scheduledTripHours;
	}

	public LocalDateTime getTimeOfArrOfLastFlight() {
		return timeOfArrOfLastFlight;
	}

	public LocalDateTime getTimeOfDepOfFirstFlight() {
		return timeOfDepOfFirstFlight;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Passenger) {
			Passenger pax = (Passenger) obj;
			if (this.paxId == pax.paxId
					&& this.destAirport.toLowerCase().trim().equals(pax.destAirport.toLowerCase().trim())) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("[paxID:").append(this.paxId).append("|destAirport:").append(this.destAirport)
				.append("|scheduledTripHours:").append(this.scheduledTripHours.toString())
				.append("|timeOfDepOfFirstFlight:").append(this.timeOfDepOfFirstFlight.toString())
				.append("|timeOfArrOfLastFlight:").append(this.timeOfArrOfLastFlight.toString()).append("]");
		return builder.toString();
	}

}
