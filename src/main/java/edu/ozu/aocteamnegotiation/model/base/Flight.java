package edu.ozu.aocteamnegotiation.model.base;

import java.time.LocalDateTime;

public class Flight {
	private String flightNumber;
	private String depAirport;
	private String arrAirport;
	private LocalDateTime scheduledTimeOfDep;
	private LocalDateTime scheduledTimeOfArr;
	private Aircraft aircraft;
	private int soldSeats;
	private LocalDateTime estimatedTimeOfDep;
	private LocalDateTime estimatedTimeOfArr;
	private int totalAvailSeats;

	private Flight() {

	}

	public static class FlightBuilder {
		private Flight flight;

		public FlightBuilder() {
			this.flight = new Flight();
		}

		public FlightBuilder flightNumber(String flightNum) {
			this.flight.flightNumber = flightNum;
			return this;
		}

		public FlightBuilder depAirport(String depAirp) {
			this.flight.depAirport = depAirp;
			return this;
		}

		public FlightBuilder arrAirport(String arrAirp) {
			this.flight.arrAirport = arrAirp;
			return this;
		}

		public FlightBuilder estTimeOfDep(LocalDateTime estTimeOfDep) {
			this.flight.estimatedTimeOfDep = estTimeOfDep;
			return this;
		}

		public FlightBuilder estTimeOfArr(LocalDateTime estTimeOfArr) {
			this.flight.estimatedTimeOfArr = estTimeOfArr;
			return this;
		}

		public FlightBuilder scheduledTimeOfDep(LocalDateTime scheduledTimeOfDep) {
			this.flight.scheduledTimeOfDep = scheduledTimeOfDep;
			return this;
		}

		public FlightBuilder scheduledTimeOfArr(LocalDateTime scheduledTimeOfArr) {
			this.flight.scheduledTimeOfArr = scheduledTimeOfArr;
			return this;
		}

		public FlightBuilder aircraft(Aircraft aircraft) {
			this.flight.aircraft = aircraft;
			return this;
		}

		public FlightBuilder soldSeats(int soldSeats) {
			this.flight.soldSeats = soldSeats;
			return this;
		}

		public FlightBuilder totalSeats(int totalAvailSeats) {
			this.flight.totalAvailSeats = totalAvailSeats;
			return this;
		}

		public Flight build() {
			return this.flight;
		}

	}

	public void setAircraft(Aircraft aircraft) {
		this.aircraft = aircraft;
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public String getDepAirport() {
		return depAirport;
	}

	public String getArrAirport() {
		return arrAirport;
	}

	public LocalDateTime getScheduledTimeOfDep() {
		return scheduledTimeOfDep;
	}

	public LocalDateTime getScheduledTimeOfArr() {
		return scheduledTimeOfArr;
	}

	public Aircraft getAircraft() {
		return aircraft;
	}

	public int getSoldSeats() {
		return soldSeats;
	}

	public LocalDateTime getEstimatedTimeOfDep() {
		return estimatedTimeOfDep;
	}

	public LocalDateTime getEstimatedTimeOfArr() {
		return estimatedTimeOfArr;
	}

	public int getTotalAvailSeats() {
		return totalAvailSeats;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("FlightNo:").append(this.flightNumber).append("|Aircraft:").append(this.aircraft)
				.append("|DepAirport:").append(this.depAirport).append("|ArrAirport:").append(this.arrAirport)
				.append("|soldSeats:").append(this.soldSeats).append("|totalAvailSeats:").append(this.totalAvailSeats);
		return builder.toString();
	}

	@Override
	public boolean equals(Object object) {
		if (object instanceof Flight) {
			Flight flight = (Flight) object;
			if (this.aircraft.equals(flight.aircraft) && this.flightNumber.equals(flight.flightNumber)) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

}
