package edu.ozu.aocteamnegotiation.model;

import java.util.ArrayList;

import edu.ozu.aocteamnegotiation.model.base.CrewMember;
import edu.ozu.aocteamnegotiation.model.base.DimensionAttribute;
import edu.ozu.aocteamnegotiation.model.constants.Constants;

public class CrewDimensionAttributes extends DimensionAttribute {
	private ArrayList<CrewMember> crewPartialSolution;

	public ArrayList<CrewMember> getCrewPartialSolution() {
		return crewPartialSolution;
	}

	public void setCrewPartialSolution(ArrayList<CrewMember> crewPartialSolution) {
		this.crewPartialSolution = crewPartialSolution;
	}

	@Override
	public String toString() {
		return Constants.CREW_DIM_ATTR_PREFIX + super.toString();
	}

}
