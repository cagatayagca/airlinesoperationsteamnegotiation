package edu.ozu.aocteamnegotiation.agents.impl;

import java.math.BigDecimal;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import edu.ozu.aocteamnegotiation.agents.IAgent;
import edu.ozu.aocteamnegotiation.model.CrewDimensionAttributes;
import edu.ozu.aocteamnegotiation.model.base.CrewActivity;
import edu.ozu.aocteamnegotiation.model.base.CrewMember;
import edu.ozu.aocteamnegotiation.model.base.DimensionAttribute;
import edu.ozu.aocteamnegotiation.model.base.Disruption;
import edu.ozu.aocteamnegotiation.model.constants.Constants;
import edu.ozu.aocteamnegotiation.util.CrewScheduleJsonParser;

public class CrewAgent implements IAgent {
	private static final Logger LOGGER = LoggerFactory.getLogger(CrewAgent.class);

	private BigDecimal maxCost = BigDecimal.ZERO;
	private Duration maxDelay = Duration.ofMillis(BigDecimal.ZERO.intValue());
	private int totalPenalty;
	private BigDecimal totalSolutionCost = BigDecimal.ZERO;
	private long totalSolutionDelay = 0;

	@Override
	public List<DimensionAttribute> createPartialSolution(Disruption disruption) {
		long previousTotalCost = BigDecimal.ZERO.longValue();
		List<DimensionAttribute> resultList = new ArrayList<>();
		ArrayList<CrewMember> disruptedCrewList = disruption.getDisruptedCrew();
		if (disruptedCrewList.isEmpty()) {
			LOGGER.info("There are no disrupted crew.");
			return Collections.emptyList();
		}
		ArrayList<CrewMember> crewScheduleList = CrewScheduleJsonParser.SCHEDULED_CREW_LIST;
		while (true) {
			crewScheduleList.removeAll(disruptedCrewList);
			for (CrewMember disruptedCrewMember : disruptedCrewList) {
				Optional<CrewMember> solutionMemberOptional = checkIfConstraintsMet(disruptedCrewMember,
						crewScheduleList);
				if (solutionMemberOptional.isPresent()) {
					disruptedCrewMember.setActivity(solutionMemberOptional.get().getActivity());
					crewScheduleList.add(disruptedCrewMember);
					this.totalPenalty += calculatePenalty(disruptedCrewMember, solutionMemberOptional.get());
					continue;
				} else {
					return Collections.emptyList();
				}

			}
			previousTotalCost = this.totalSolutionDelay + this.totalPenalty + this.totalSolutionCost.longValue();
			calculateMaxCostAndDelay(crewScheduleList);
			calculateTotalCostAndDelay(crewScheduleList);
			long currentTotalCost = this.totalSolutionDelay + this.totalPenalty + this.totalSolutionCost.longValue();
			CrewDimensionAttributes dimensionAttributes = new CrewDimensionAttributes();
			dimensionAttributes.setCost(this.totalSolutionCost);
			dimensionAttributes.setDelay(this.totalSolutionDelay);
			dimensionAttributes.setPenalty(this.totalPenalty);
			resultList.add(dimensionAttributes);

			if (currentTotalCost < previousTotalCost) {
				break;
			} else {
				continue;
			}
		}

		return resultList;
	}

	private void calculateMaxCostAndDelay(List<CrewMember> crewList) {
		Duration maxDelay = this.maxDelay;
		BigDecimal maxCost = this.maxCost;
		for (CrewMember crewMember : crewList) {
			Duration currentDelay = Duration.between(crewMember.getActivity().getEstStartTime(),
					crewMember.getActivity().getScheduledStartTime());
			if (currentDelay.compareTo(maxDelay) == Constants.BIGGER_THAN) {
				maxDelay = currentDelay;
			}
			BigDecimal currentCost = calculateCost(crewMember);
			if (currentCost.compareTo(maxCost) == Constants.BIGGER_THAN) {
				maxCost = currentCost;
			}
		}
		this.maxDelay = maxDelay;
		this.maxCost = maxCost;
	}

	private Optional<CrewMember> checkIfConstraintsMet(CrewMember disruptedCrewMember,
			List<CrewMember> crewMemberList) {
		CrewMember solutionCrewMember = null;
		for (CrewMember scheduledCrewMember : crewMemberList) {
			if (scheduledCrewMember.getRank() == disruptedCrewMember.getRank()) {
				CrewActivity disruptedActivity = scheduledCrewMember.getActivity();
				CrewActivity scheduledActivity = disruptedCrewMember.getActivity();
				if (scheduledActivity != null && disruptedActivity != null) {
					if (StringUtils.equalsIgnoreCase(scheduledActivity.getStartingLoc().trim(),
							disruptedActivity.getStartingLoc().trim())) {
						if (scheduledActivity.getScheduledStartTime().isAfter(disruptedActivity.getEstStartTime())) {
							if (disruptedCrewMember.getDailyHours() < disruptedCrewMember.getRank()
									.getDailyMaxHours()) {
								solutionCrewMember = scheduledCrewMember;
								break;
							}
						}
					}
				}
			} else {
				continue;
			}
		}
		return Optional.ofNullable(solutionCrewMember);
	}

	private BigDecimal calculateCost(CrewMember member) {
		BigDecimal cost = BigDecimal.ZERO;
		if (member != null) {
			int activityDuration = Math.toIntExact(Duration
					.between(member.getActivity().getScheduledStartTime(), member.getActivity().getScheduledEndTime())
					.toHours());
			int extraTimeSpent = (member.getDailyHours() + activityDuration) - member.getRank().getDailyMaxHours();
			if (extraTimeSpent > BigDecimal.ZERO.intValue()) {
				cost.add(new BigDecimal(extraTimeSpent * member.getRank().getExtraHourRate()));
				cost.add(new BigDecimal((member.getRank().getDailyMaxHours() - member.getDailyHours())
						* member.getRank().getHourlyRate()));
			} else {
				cost.add(new BigDecimal(Math
						.toIntExact(Duration.between(member.getActivity().getScheduledEndTime(),
								member.getActivity().getScheduledStartTime()).toHours())
						* member.getRank().getHourlyRate()));
			}
		}
		return cost;
	}

	private void calculateTotalCostAndDelay(List<CrewMember> crewList) {
		BigDecimal totalCost = BigDecimal.ZERO;
		long totalDelay = BigDecimal.ZERO.longValue();
		if (!CollectionUtils.isEmpty(crewList)) {
			for (CrewMember crewMember : crewList) {
				if (crewMember.getActivity() != null) {
					Duration currentDelay = Duration.between(crewMember.getActivity().getEstStartTime(),
							crewMember.getActivity().getScheduledStartTime());
					totalDelay += (currentDelay.toMillis() / this.maxDelay.toMillis());
				}

			}
		}
		this.totalSolutionCost = totalCost;
		this.totalSolutionDelay = totalDelay;

	}

	private int calculatePenalty(CrewMember disruptedMember, CrewMember scheduledMember) {
		int penalty = 0;
		boolean isPenalizedTwice = false;
		if (disruptedMember != null || scheduledMember != null) {
			if (!StringUtils.equalsIgnoreCase(disruptedMember.getHomeBase(), scheduledMember.getHomeBase())) {
				penalty++;
			}
			if (disruptedMember.getActivity().getEstEndTime()
					.isAfter(scheduledMember.getActivity().getScheduledStartTime())
					|| (scheduledMember.getActivity().getScheduledStartTime()
							.isBefore(disruptedMember.getActivity().getEstStartTime())
							&& disruptedMember.getActivity().getEstStartTime()
									.isBefore(scheduledMember.getActivity().getScheduledEndTime()))) {
				penalty++;
				isPenalizedTwice = true;
			}
			if (scheduledMember.getActivity().getScheduledStartTime()
					.isAfter(disruptedMember.getActivity().getScheduledStartTime())
					&& disruptedMember.getActivity().getEstEndTime()
							.isAfter(scheduledMember.getActivity().getScheduledEndTime())) {
				if (!isPenalizedTwice) {
					penalty++;
				}
			}
		}
		return penalty;
	}

}
