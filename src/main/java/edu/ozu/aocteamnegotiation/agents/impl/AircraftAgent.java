package edu.ozu.aocteamnegotiation.agents.impl;

import java.math.BigDecimal;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.ozu.aocteamnegotiation.agents.IAgent;
import edu.ozu.aocteamnegotiation.model.AircraftDimensionAttributes;
import edu.ozu.aocteamnegotiation.model.base.DimensionAttribute;
import edu.ozu.aocteamnegotiation.model.base.Disruption;
import edu.ozu.aocteamnegotiation.model.base.Flight;
import edu.ozu.aocteamnegotiation.model.constants.Constants;
import edu.ozu.aocteamnegotiation.util.FlightCostsJsonParser;
import edu.ozu.aocteamnegotiation.util.FlightScheduleJsonParser;

public class AircraftAgent implements IAgent {

	private static final Logger LOGGER = LoggerFactory.getLogger(AircraftAgent.class);
	private BigDecimal maxCost = BigDecimal.ZERO;
	private Duration maxDelay = Duration.ofMillis(1);
	private int totalPenalty;
	private BigDecimal totalSolutionCost = BigDecimal.ZERO;
	private long totalSolutionDelay = 0;

	@Override
	public List<DimensionAttribute> createPartialSolution(Disruption disruption) {
		long previousTotalCost = 0;
		List<DimensionAttribute> result = new ArrayList<>();
		ArrayList<Flight> scheduledFlightList = FlightScheduleJsonParser.SCHEDULED_FLIGHT_LIST;
		Flight disruptedFlight = disruption.getDisruptedFlight();
		while (true) {
			scheduledFlightList.remove(disruptedFlight);
			for (Flight scheduledFlight : scheduledFlightList) {
				if (disruptedFlight.equals(scheduledFlight)) {
					continue;
				}
				if (StringUtils.equals(disruptedFlight.getDepAirport(), scheduledFlight.getDepAirport())
						&& scheduledFlight.getScheduledTimeOfDep().isAfter(disruptedFlight.getScheduledTimeOfDep())) {
					disruptedFlight.setAircraft(scheduledFlight.getAircraft());
					this.totalPenalty += calculatePenalty(disruptedFlight, scheduledFlight);
					scheduledFlightList.add(disruptedFlight);// partially solved flight is added back to the schedule.
				}
			}
			previousTotalCost = this.totalSolutionDelay + this.totalPenalty + this.totalSolutionCost.longValue();
			calculateMaxCostAndDelay(scheduledFlightList);
			calculateTotalCostAndDelay(scheduledFlightList);
			long currentTotalCost = this.totalSolutionDelay + this.totalPenalty + this.totalSolutionCost.longValue();

			AircraftDimensionAttributes dimensionAttributes = new AircraftDimensionAttributes();
			dimensionAttributes.setAircraftPartialSolution(scheduledFlightList);
			dimensionAttributes.setCost(this.totalSolutionCost);
			dimensionAttributes.setDelay(this.totalSolutionDelay);
			dimensionAttributes.setPenalty(this.totalPenalty);
			result.add(dimensionAttributes);

			if (currentTotalCost < previousTotalCost) {
				break;
			} else {
				continue;
			}
		}

		return result;
	}

	private void calculateMaxCostAndDelay(ArrayList<Flight> scheduledFlightList) {
		Duration maxDelay = this.maxDelay;
		BigDecimal maxCost = this.maxCost;
		for (Flight flight : scheduledFlightList) {
			if (FlightCostsJsonParser.FLIGHT_COSTS_MAP.get(flight.getDepAirport()) != null
					&& FlightCostsJsonParser.FLIGHT_COSTS_MAP.get(flight.getArrAirport()) != null) {
				BigDecimal totalCostPerFlight = FlightCostsJsonParser.FLIGHT_COSTS_MAP.get(flight.getDepAirport())
						.totalFlightCost()
						.add(FlightCostsJsonParser.FLIGHT_COSTS_MAP.get(flight.getArrAirport()).totalFlightCost());
				if (totalCostPerFlight.compareTo(maxCost) == Constants.BIGGER_THAN) {
					maxCost = totalCostPerFlight;
				}
			} else {
				LOGGER.debug(new StringBuilder().append("Unknown AIRPORTs:").append(flight.getDepAirport()).append("|")
						.append(flight.getArrAirport()).toString());
			}
			Duration delayPerFlight = Duration.between(flight.getEstimatedTimeOfDep(), flight.getScheduledTimeOfDep());
			if (delayPerFlight.compareTo(maxDelay) == 1) {
				maxDelay = delayPerFlight;
			}
		}
		this.maxCost = maxCost;
		this.maxDelay = maxDelay;

	}

	private int calculatePenalty(Flight disruptedFlight, Flight scheduledFlight) {
		int penalty = 0;
		if (disruptedFlight != null && scheduledFlight != null) {
			if (disruptedFlight.getAircraft().getFleetNumber() != scheduledFlight.getAircraft().getFleetNumber()) {
				penalty++;
			}
			if (disruptedFlight.getTotalAvailSeats() < scheduledFlight.getSoldSeats()) {
				penalty++;
			}
			if (disruptedFlight.getSoldSeats() > scheduledFlight.getTotalAvailSeats()) {
				penalty++;
			}
		}
		return penalty;
	}

	private void calculateTotalCostAndDelay(ArrayList<Flight> flightList) {
		BigDecimal totalCost = BigDecimal.ZERO;
		long totalDelay = BigDecimal.ZERO.longValue();
		for (Flight flight : flightList) {
			if (flight != null) {
				if (flight.getDepAirport() != null && flight.getArrAirport() != null) {
					if (FlightCostsJsonParser.FLIGHT_COSTS_MAP.get(flight.getDepAirport()) != null
							&& FlightCostsJsonParser.FLIGHT_COSTS_MAP.get(flight.getArrAirport()) != null) {
						totalCost.add(FlightCostsJsonParser.FLIGHT_COSTS_MAP.get(flight.getDepAirport())
								.totalFlightCost().add(FlightCostsJsonParser.FLIGHT_COSTS_MAP
										.get(flight.getArrAirport()).totalFlightCost())
								.divide(this.maxCost));
					}
				}
				if (flight.getEstimatedTimeOfDep() != null && flight.getScheduledTimeOfDep() != null) {
					Duration delayPerFlight = Duration.between(flight.getEstimatedTimeOfDep(),
							flight.getScheduledTimeOfDep());
					totalDelay += (delayPerFlight.toMillis() / this.maxDelay.toMillis());
				}
			}
		}
		this.totalSolutionCost = totalCost;
		this.totalSolutionDelay = totalDelay;

	}
}
