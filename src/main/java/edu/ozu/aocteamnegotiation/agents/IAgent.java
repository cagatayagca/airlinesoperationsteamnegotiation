package edu.ozu.aocteamnegotiation.agents;

import java.util.List;

import edu.ozu.aocteamnegotiation.model.base.DimensionAttribute;
import edu.ozu.aocteamnegotiation.model.base.Disruption;

public interface IAgent {
	List<DimensionAttribute> createPartialSolution(Disruption disruption);

}
