package edu.ozu.aocteamnegotiation.util;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.stream.JsonReader;

import edu.ozu.aocteamnegotiation.model.base.FlightCost;
import edu.ozu.aocteamnegotiation.model.constants.Constants;
import edu.ozu.aocteamnegotiation.model.constants.ErrorMessages;

public class FlightCostsJsonParser {

	private static final Logger LOGGER = LoggerFactory.getLogger(FlightCostsJsonParser.class);
	public static HashMap<String, FlightCost> FLIGHT_COSTS_MAP = new HashMap<>();

	private FlightCostsJsonParser() {
	}

	public static void parseFlightCostJson(String pathToJsonFile) {
		try (FileReader fileReader = new FileReader(pathToJsonFile)) {
			JsonReader jsonReader = new JsonReader(fileReader);
			JsonArray jsonArray = Constants.GSON.fromJson(jsonReader, JsonArray.class);
			for (JsonElement jsonElement : jsonArray) {
				String airport = jsonElement.getAsJsonObject().get(Constants.AIRPORT).getAsString();
				FlightCost flightCost = new FlightCost.FlightCostBuilder(airport)
						.airTrafficContCharges(
								jsonElement.getAsJsonObject().get(Constants.ATCCHARGES).getAsBigDecimal())
						.fuel(jsonElement.getAsJsonObject().get(Constants.FUEL).getAsBigDecimal())
						.handling(jsonElement.getAsJsonObject().get(Constants.HANDLING).getAsBigDecimal())
						.landing(jsonElement.getAsJsonObject().get(Constants.LANDING).getAsBigDecimal())
						.maintenance(jsonElement.getAsJsonObject().get(Constants.MAINT).getAsBigDecimal())
						.parking(jsonElement.getAsJsonObject().get(Constants.PARKING).getAsBigDecimal())
						.takeOff(jsonElement.getAsJsonObject().get(Constants.TAKEOFF).getAsBigDecimal()).build();
				FLIGHT_COSTS_MAP.putIfAbsent(airport, flightCost);
			}
		} catch (FileNotFoundException e) {
			LOGGER.error(ErrorMessages.FLIGHT_COST_JSON_OPENING_ERR_MSG, e);
			// TODO delete prints
			e.printStackTrace();
		} catch (IOException e) {
			LOGGER.error(ErrorMessages.FLIGHT_COST_JSON_READING_ERR_MSG, e);
			// TODO delete prints
			e.printStackTrace();
		} catch (Exception e) {
			LOGGER.error(ErrorMessages.FLIGHT_COST_JSON_UNKNOWN_ERR_MSG, e);
			// TODO delete prints
			e.printStackTrace();
		}
	}

}
