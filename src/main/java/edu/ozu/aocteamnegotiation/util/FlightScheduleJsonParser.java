package edu.ozu.aocteamnegotiation.util;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonSyntaxException;
import com.google.gson.stream.JsonReader;

import edu.ozu.aocteamnegotiation.model.base.Flight;
import edu.ozu.aocteamnegotiation.model.constants.Constants;
import edu.ozu.aocteamnegotiation.model.constants.ErrorMessages;

public final class FlightScheduleJsonParser {

	private static final Logger LOGGER = LoggerFactory.getLogger(FlightScheduleJsonParser.class);
	public static ArrayList<Flight> SCHEDULED_FLIGHT_LIST = new ArrayList<>();

	private FlightScheduleJsonParser() {
	}

	public static void parseFlightScheduleJson(String pathToJsonFile) {
		ArrayList<Flight> flightSchduleList = new ArrayList<>();
		try (FileReader fileReader = new FileReader(pathToJsonFile)) {
			JsonReader jsonReader = new JsonReader(fileReader);
			JsonArray jsonArray = Constants.GSON.fromJson(jsonReader, JsonArray.class);
			for (JsonElement jsonElement : jsonArray) {
				if (!FlightJsonParser.parseFlightJson(jsonElement).isPresent()) {
					throw new RuntimeException(ErrorMessages.NULL_FLIGHT_RUNTIME_ERR_MSG);
				}
				Flight flight = FlightJsonParser.parseFlightJson(jsonElement)
						.orElse(new Flight.FlightBuilder().build());
				flightSchduleList.add(flight);
			}
		} catch (FileNotFoundException e) {
			LOGGER.error(ErrorMessages.FLIGHT_SCHEDULE_JSON_OPENING_ERR_MSG, e);

			// TODO print must be deleted.
			e.printStackTrace();
		} catch (IOException e) {
			LOGGER.error(ErrorMessages.FLIGHT_SCHEDULE_JSON_READING_ERR_MSG, e);
			// TODO print must be deleted.
			e.printStackTrace();
		} catch (JsonSyntaxException e) {
			LOGGER.error(ErrorMessages.FLIGHT_SCHEDULE_JSON_SYNTAX_ERR_MSG, e);
		} catch (Exception e) {
			LOGGER.error(ErrorMessages.FLIGHT_SCHEDULE_JSON_UNKNOWN_ERR_MSG, e);
			// TODO delete printstacktrace
			e.printStackTrace();
		}
		if (flightSchduleList.isEmpty()) {
			SCHEDULED_FLIGHT_LIST = new ArrayList<>(Collections.emptyList());
		}
		SCHEDULED_FLIGHT_LIST = flightSchduleList;
	}

}
