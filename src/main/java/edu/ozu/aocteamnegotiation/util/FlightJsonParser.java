package edu.ozu.aocteamnegotiation.util;

import java.time.LocalDateTime;
import java.util.Optional;

import com.google.gson.JsonElement;

import edu.ozu.aocteamnegotiation.model.base.Aircraft;
import edu.ozu.aocteamnegotiation.model.base.Flight;
import edu.ozu.aocteamnegotiation.model.constants.Constants;
import edu.ozu.aocteamnegotiation.model.constants.ErrorMessages;

public final class FlightJsonParser {

	private FlightJsonParser() {
	}

	public static Optional<Flight> parseFlightJson(JsonElement flightJsonElem) {
		if (flightJsonElem == null) {
			throw new RuntimeException(ErrorMessages.FLIGHT_JSON_NULL_RUNTIME_ERR_MSG);
		}
		JsonElement aircraftJsonElement = flightJsonElem.getAsJsonObject().get(Constants.AIRCRAFT);
		Aircraft aircraft = new Aircraft(aircraftJsonElement.getAsJsonObject().get(Constants.FLEET_NUM).getAsInt(),
				aircraftJsonElement.getAsJsonObject().get(Constants.TAIL_NUMBER).getAsString());
		Flight flight = new Flight.FlightBuilder()
				.flightNumber(flightJsonElem.getAsJsonObject().get(Constants.FLIGHT_NUMBER).getAsString())
				.depAirport(flightJsonElem.getAsJsonObject().get(Constants.DEPARTURE_AIRPORT).getAsString())
				.arrAirport(flightJsonElem.getAsJsonObject().get(Constants.ARRIVAL_AIRPORT).getAsString())
				.scheduledTimeOfDep(LocalDateTime.parse(
						flightJsonElem.getAsJsonObject().get(Constants.SCHEDULED_TIME_OF_DEP).getAsString(),
						Constants.DATETIME_FORMATTER))
				.scheduledTimeOfArr(LocalDateTime.parse(
						flightJsonElem.getAsJsonObject().get(Constants.SCHEDULED_TIME_OF_ARR).getAsString(),
						Constants.DATETIME_FORMATTER))
				.estTimeOfDep(LocalDateTime.parse(
						flightJsonElem.getAsJsonObject().get(Constants.ESTIMATED_TIME_OF_DEP).getAsString(),
						Constants.DATETIME_FORMATTER))
				.estTimeOfArr(LocalDateTime.parse(
						flightJsonElem.getAsJsonObject().get(Constants.ESTIMATED_TIME_OF_ARR).getAsString(),
						Constants.DATETIME_FORMATTER))
				.soldSeats(flightJsonElem.getAsJsonObject().get(Constants.SOLD_SEATS).getAsInt())
				.totalSeats(flightJsonElem.getAsJsonObject().get(Constants.TOTAL_AVAIL_SEATS).getAsInt())
				.aircraft(aircraft).build();
		return Optional.ofNullable(flight);
	}

}
