package edu.ozu.aocteamnegotiation.util;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.JsonElement;
import com.google.gson.stream.JsonReader;

import edu.ozu.aocteamnegotiation.model.base.CrewMember;
import edu.ozu.aocteamnegotiation.model.base.Disruption;
import edu.ozu.aocteamnegotiation.model.base.Flight;
import edu.ozu.aocteamnegotiation.model.base.Passenger;
import edu.ozu.aocteamnegotiation.model.constants.Constants;
import edu.ozu.aocteamnegotiation.model.constants.ErrorMessages;

public class DisruptionJsonParser {

	private static final Logger LOGGER = LoggerFactory.getLogger(DisruptionJsonParser.class);

	private DisruptionJsonParser() {
	}

	public static Optional<Disruption> parseDisruptionJson(String pathToJsonFile) {
		Disruption disruption = null;
		try (FileReader fileReader = new FileReader(pathToJsonFile)) {
			JsonReader jsonReader = new JsonReader(fileReader);
			JsonElement disruptionJsonElem = Constants.GSON.fromJson(jsonReader, JsonElement.class);
			if (!FlightJsonParser.parseFlightJson(disruptionJsonElem.getAsJsonObject().get(Constants.DISRUPTED_FLIGHT))
					.isPresent()) {
				throw new RuntimeException(ErrorMessages.NULL_FLIGHT_RUNTIME_ERR_MSG);
			}
			Flight disruptedFlight = FlightJsonParser
					.parseFlightJson(disruptionJsonElem.getAsJsonObject().get(Constants.DISRUPTED_FLIGHT))
					.orElse(new Flight.FlightBuilder().build());
			List<CrewMember> disruptedCrewList = new ArrayList<>(CrewJsonParser.parseCrewJson(
					disruptionJsonElem.getAsJsonObject().get(Constants.DISRUPTED_CREW).getAsJsonArray()));
			List<Passenger> disruptedPaxList = new ArrayList<>(PaxJsonParser.parsePaxJson(
					disruptionJsonElem.getAsJsonObject().get(Constants.DISRUPTED_PAX_LIST).getAsJsonArray()));
			disruption = new Disruption(disruptedFlight, disruptedCrewList, disruptedPaxList);
		} catch (FileNotFoundException e) {
			LOGGER.error(ErrorMessages.DISRUPTION_JSON_OPENING_ERR_MSG, e);
			// TODO delete prints
			e.printStackTrace();
		} catch (IOException e) {
			LOGGER.error(ErrorMessages.DISRUPTION_JSON_READING_ERR_MSG, e);
			// TODO delete prints
			e.printStackTrace();
		} catch (Exception e) {
			LOGGER.error(ErrorMessages.DISRUPTION_JSON_UNKNOWN_ERR_MSG, e);
			// TODO delete prints
			e.printStackTrace();
		}
		return Optional.ofNullable(disruption);
	}

}
