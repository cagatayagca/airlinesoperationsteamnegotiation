package edu.ozu.aocteamnegotiation.util;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.JsonArray;
import com.google.gson.JsonSyntaxException;
import com.google.gson.stream.JsonReader;

import edu.ozu.aocteamnegotiation.model.base.CrewMember;
import edu.ozu.aocteamnegotiation.model.constants.Constants;
import edu.ozu.aocteamnegotiation.model.constants.ErrorMessages;

public final class CrewScheduleJsonParser {

	private CrewScheduleJsonParser() {
	}

	private static final Logger LOGGER = LoggerFactory.getLogger(CrewScheduleJsonParser.class);
	public static ArrayList<CrewMember> SCHEDULED_CREW_LIST = new ArrayList<>();

	public static void parseCrewScheduleJson(String pathToJsonFile) {
		List<CrewMember> crewList = new ArrayList<>();
		try (FileReader fileReader = new FileReader(pathToJsonFile)) {
			JsonReader jsonReader = new JsonReader(fileReader);
			JsonArray crewJsonArray = Constants.GSON.fromJson(jsonReader, JsonArray.class);
			if (crewJsonArray == null) {
				LOGGER.debug(ErrorMessages.CREW_JSON_EMPTY_ERR_MSG);
			}
			crewList = CrewJsonParser.parseCrewJson(crewJsonArray);
		} catch (FileNotFoundException e) {
			LOGGER.error(ErrorMessages.CREW_SCHEDULE_JSON_OPENING_ERR_MSG, e);
			// TODO print must be deleted.
			e.printStackTrace();
		} catch (IOException e) {
			LOGGER.error(ErrorMessages.CREW_SCHEDULE_JSON_READING_ERR_MSG, e);
			// TODO print must be deleted.
			e.printStackTrace();
		} catch (JsonSyntaxException e) {
			LOGGER.error(ErrorMessages.CREW_SCHEDULE_JSON_SYNTAX_ERR_MSG, e);
		} catch (Exception e) {
			LOGGER.error(ErrorMessages.CREW_JSON_PARSE_ERROR, e);
			e.printStackTrace();
			// TODO delete printout
		}
		if (crewList.isEmpty()) {
			SCHEDULED_CREW_LIST = new ArrayList<>(Collections.emptyList());
		} else {
			SCHEDULED_CREW_LIST = new ArrayList<>(crewList);
		}

	}
}
