package edu.ozu.aocteamnegotiation.util;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;

import edu.ozu.aocteamnegotiation.model.base.CrewActivity;
import edu.ozu.aocteamnegotiation.model.base.CrewMember;
import edu.ozu.aocteamnegotiation.model.constants.Constants;
import edu.ozu.aocteamnegotiation.model.constants.ErrorMessages;
import edu.ozu.aocteamnegotiation.model.enums.CrewMemberRanks;

public final class CrewJsonParser {
	private CrewJsonParser() {
	}

	private static final Logger LOGGER = LoggerFactory.getLogger(CrewJsonParser.class);

	public static List<CrewMember> parseCrewJson(JsonArray paxJsonArray) {
		CrewMember crewMember = null;
		List<CrewMember> crewList = new ArrayList<>();
		if (paxJsonArray == null) {
			LOGGER.debug(ErrorMessages.CREW_JSON_EMPTY_ERR_MSG);
		}
		for (JsonElement crewMemberJson : paxJsonArray) {
			JsonElement crewActivityJsonElem = crewMemberJson.getAsJsonObject().get(Constants.ACTIVITY);
			CrewActivity activity = new CrewActivity.CrewActivityBuilder()
					.startingLocation(crewActivityJsonElem.getAsJsonObject().get(Constants.STARTING_LOC).getAsString())
					.estStartTime(LocalDateTime.parse(
							crewActivityJsonElem.getAsJsonObject().get(Constants.ESTIMATED_START_TIME).getAsString(),
							Constants.DATETIME_FORMATTER))
					.estEndTime(LocalDateTime.parse(
							crewActivityJsonElem.getAsJsonObject().get(Constants.EST_END_TIME).getAsString(),
							Constants.DATETIME_FORMATTER))
					.scheduledStartTime(LocalDateTime.parse(
							crewActivityJsonElem.getAsJsonObject().get(Constants.SCHEDULED_START_TIME).getAsString(),
							Constants.DATETIME_FORMATTER))
					.scheduledEndTime(LocalDateTime.parse(
							crewActivityJsonElem.getAsJsonObject().get(Constants.SCHEDULED_END_TIME).getAsString(),
							Constants.DATETIME_FORMATTER))
					.build();
			crewMember = new CrewMember.CrewMemberBuilder()
					.fleetNo(crewMemberJson.getAsJsonObject().get(Constants.FLEET_NUM).getAsInt())
					.memberId(crewMemberJson.getAsJsonObject().get(Constants.MEMBER_ID).getAsInt())
					.homeBase(crewMemberJson.getAsJsonObject().get(Constants.HOMEBASE).getAsString())
					.dailyHours(crewMemberJson.getAsJsonObject().get(Constants.DAILY_HOURS).getAsInt())
					.readinessTime(LocalDateTime.parse(
							crewMemberJson.getAsJsonObject().get(Constants.READINESS_TIME).getAsString(),
							Constants.DATETIME_FORMATTER))
					.rank(findRank(crewMemberJson.getAsJsonObject().get(Constants.RANK).getAsString()))
					.activity(activity).build();

			if (crewMember != null) {
				crewList.add(crewMember);
			}
		}

		return crewList == null ? Collections.emptyList() : crewList;

	}

	static CrewMemberRanks findRank(String rankEnum) {
		switch (rankEnum) {
		case Constants.CAPTAIN:
			return CrewMemberRanks.CAPTAIN;
		case Constants.FLIGHT_ATTENDANT:
			return CrewMemberRanks.FLIGHT_ATTENDANT;
		case Constants.PURSER:
			return CrewMemberRanks.PURSER;
		case Constants.OFFICER_1ST:
			return CrewMemberRanks.OFFICER_2ND;
		default:
			return CrewMemberRanks.FLIGHT_ATTENDANT;
		}
	}
}
