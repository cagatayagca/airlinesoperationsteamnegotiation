package edu.ozu.aocteamnegotiation.util;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;

import edu.ozu.aocteamnegotiation.model.base.Passenger;
import edu.ozu.aocteamnegotiation.model.constants.Constants;
import edu.ozu.aocteamnegotiation.model.constants.ErrorMessages;

public final class PaxJsonParser {
	private PaxJsonParser() {
	}

	private static final Logger LOGGER = LoggerFactory.getLogger(PaxJsonParser.class);

	public static List<Passenger> parsePaxJson(JsonArray paxJsonArray) {
		if (paxJsonArray == null) {
			LOGGER.debug(ErrorMessages.PAX_JSON_EMPTY_ERR_MSG);
			return Collections.emptyList();
		}
		Passenger passenger = null;
		ArrayList<Passenger> paxList = new ArrayList<>();
		try {
			for (JsonElement disruptedPaxJsonObj : paxJsonArray) {
				if (disruptedPaxJsonObj != null) {
					passenger = new Passenger.PaxBuilder(
							disruptedPaxJsonObj.getAsJsonObject().get(Constants.PAX_ID).getAsInt()).destinationAirp(
									disruptedPaxJsonObj.getAsJsonObject().get(Constants.DEST_AIRPORT).getAsString())
									.scheduledTripHours(disruptedPaxJsonObj.getAsJsonObject()
											.get(Constants.SCHEDULED_TRIP_HOURS).getAsBigInteger())
									.timeOfDepature(LocalDateTime.parse(
											disruptedPaxJsonObj.getAsJsonObject()
													.get(Constants.TIME_OF_DEP_OF_FIRST_FLIGHT).getAsString(),
											Constants.DATETIME_FORMATTER))
									.timeOfArrival(LocalDateTime.parse(
											disruptedPaxJsonObj.getAsJsonObject()
													.get(Constants.TIME_OF_ARR_OF_LAST_FLIGHT).getAsString(),
											Constants.DATETIME_FORMATTER))
									.build();
				}
				if (passenger != null) {
					paxList.add(passenger);
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while parsing pax json array. {}", e);
			e.printStackTrace();
			// TODO delete printout
		}

		return paxList == null ? Collections.emptyList() : paxList;
	}
}
